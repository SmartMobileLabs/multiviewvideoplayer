package com.smartmobilelabs.multiviewvideoplayer;

import com.smartmobilelabs.evo.android.coresdk.EVO;
import com.smartmobilelabs.evo.android.coresdk.EVOConfiguration;
import com.smartmobilelabs.evo.android.coresdk.LogLevel;

public class EVOProvider {

    private static EVOProvider INSTANCE;

    private static final String EVO_URL = "https://evo1azuweu.smartmobilelabs.com:9000/LTECamOrchestrator";
    private static final String EVO_USER_NAME = "admin";
    private static final String EVO_PASSWORD = "Sml1Orchestrator";

    private static String deviceId;
    private final EVO evo;

    /**
     * Gets the singleton instance of the EVOProvider.
     *
     * @return the instance
     */
    public static EVOProvider newInstance()
    {
        if (INSTANCE == null) {
            INSTANCE = new EVOProvider();
        }
        return INSTANCE;
    }

    private EVOProvider() {
        EVOConfiguration evoConfiguration = new EVOConfiguration();
        evoConfiguration.setUrl(EVO_URL);
        evoConfiguration.setUsername(EVO_USER_NAME);
        evoConfiguration.setPassword(EVO_PASSWORD);
        evoConfiguration.setEnableWifiMulticast(false);
        evoConfiguration.setEnableEmbms(false);
        evoConfiguration.setDeviceId(deviceId);

        evo = new EVO(evoConfiguration);
        evo.setEvoLogLevel(LogLevel.DEBUG);
    }

    /**
     * Gets the singleton instance of EVO.
     *
     * @return the evo
     */
    public EVO getEvo() {
        return evo;
    }

    public static void setDeviceId(String d) {
        deviceId = d;
    }
}

