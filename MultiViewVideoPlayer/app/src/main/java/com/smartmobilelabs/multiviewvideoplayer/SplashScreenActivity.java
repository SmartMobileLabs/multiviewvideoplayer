package com.smartmobilelabs.multiviewvideoplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class SplashScreenActivity extends AppCompatActivity {

    private static final String TAG = "SplashScreenActivity";
    private static final int PERMISSION_REQUEST_CODE= 1024;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        checkPermissions();
    }

    private void navigateToMain() {
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
            startActivity(intent);

            // close this activity
            finish();
        }, 2000);
    }

    private void checkPermissions() {
        int internet = ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        if (internet == PackageManager.PERMISSION_GRANTED) {
            // do something
            navigateToMain();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                                Manifest.permission.INTERNET,
                        },
                        PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if(requestCode == PERMISSION_REQUEST_CODE
                && grantResults[2] == PackageManager.PERMISSION_GRANTED
        ){
            Log.d(TAG,"permissions granted");

            navigateToMain();
        } else {
            Log.d(TAG,"permissions not granted");
        }
    }
}