package com.smartmobilelabs.multiviewvideoplayer;

import android.app.Application;

import com.smartmobilelabs.evo.android.coresdk.EVO;

public class DemoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        /*
         need no initialize EVO with the current application
         */
        EVO.init(this);
    }
}
