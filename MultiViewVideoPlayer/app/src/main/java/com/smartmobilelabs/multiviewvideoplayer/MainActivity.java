package com.smartmobilelabs.multiviewvideoplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.smartmobilelabs.evo.android.platform.Fault;
import com.smartmobilelabs.evo.android.playersdk.player.Slot.Slot;
import com.smartmobilelabs.evo.android.playersdk.player.fragment.MultiView.ActivityUtilities;
import com.smartmobilelabs.evo.android.playersdk.player.fragment.MultiView.MultiView;
import com.smartmobilelabs.evo.android.playersdk.player.fragment.MultiView.MultiViewActivityInterface;
import com.smartmobilelabs.evo.android.playersdk.player.fragment.MultiView.MultiViewConfiguration;
import com.smartmobilelabs.evo.android.playersdk.player.fragment.MultiView.SMLMultiViewFragment;
import com.smartmobilelabs.evo.android.playersdk.player.util.EncodingToRTPLatencyMS;
import com.smartmobilelabs.evo.android.playersdk.player.util.VideoViewParameters;
import com.smartmobilelabs.evo.android.playersdk.player.view.SmlMediaPlayerManager;

public class MainActivity extends AppCompatActivity implements MultiViewActivityInterface {

    private static final String TAG = "MainActivity";

    /**
     * toolbar
     */
    private Toolbar myToolbar;

    /**
     * multiView fragment
     */
    private SMLMultiViewFragment mMultiViewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();

        mMultiViewFragment = (SMLMultiViewFragment) getSupportFragmentManager().findFragmentById(R.id.multi_view_fragment);

        setEvoDeviceId();
        setupMultiViewFragment();
    }

    private void setupMultiViewFragment() {
        MultiViewConfiguration mvc = createMultiViewFragmentConfigurations();
        mMultiViewFragment.onConfigurationChanged(mvc);
        mMultiViewFragment.setActivity(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            refreshCameras();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * setup the toolbar
     */
    private void setupToolbar() {
        myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
    }

    /**
     * this method is needed for the multiView fragment to handle
     * the orientation when changing from multi-view to single view
     *
     * @param orientation orientation id
     */
    public void setRequestedOrientation(int orientation) {
        super.setRequestedOrientation(orientation);
    }

    /**
     * pass the current device ID to EVO
     */
    private void setEvoDeviceId() {
        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceId = "demo_app_" + android_id;

        EVOProvider.setDeviceId(deviceId);
    }

    /**
     * create configurations for the multiView fragment
     *
     * @return {@link MultiViewConfiguration}
     */
    private MultiViewConfiguration createMultiViewFragmentConfigurations() {
        MultiViewConfiguration multiViewConfiguration = new MultiViewConfiguration();

        // general params
        multiViewConfiguration.setEvo(EVOProvider.newInstance().getEvo());
        multiViewConfiguration.setNumberOfView(4);
        multiViewConfiguration.setStatisticsViewEnabled(false);
        multiViewConfiguration.setFastModeEnabled(false);
        multiViewConfiguration.setMainViewEnabled(false);
        multiViewConfiguration.setMainViewAudioEnabled(false);
        multiViewConfiguration.setVrLensDistance(54);
        multiViewConfiguration.setUserProfile(Slot.USER_PROFILE.HD_SD_AUDIO);

        EncodingToRTPLatencyMS ep  = new EncodingToRTPLatencyMS();
        ep.encodeLatencyValue = 0;
        multiViewConfiguration.setEncodingToRTPLatencyMS(ep);

        // video params
        VideoViewParameters videoViewParameters = new VideoViewParameters();
        videoViewParameters.infBufferEnabled = false;
        videoViewParameters.maxBufferKiB = 400;
        videoViewParameters.mediaCodecEnabled = false;
        multiViewConfiguration.setVideoViewParams(videoViewParameters);
        multiViewConfiguration.setNalTargetBufferSize(1);
        multiViewConfiguration.setMaxNalAgeMs(3000);
        multiViewConfiguration.setmPacketTimeOut(3);
        multiViewConfiguration.setmPacketOverloadCount(450);
        multiViewConfiguration.setmRenderBufferSize(2);

        // player
        multiViewConfiguration.setPlayerToCreate(SmlMediaPlayerManager.PlayerToCreate.LD);
        multiViewConfiguration.setForce_ld_player(true);

        return multiViewConfiguration;
    }

    /**
     * refresh the cameras list
     */
    private void refreshCameras() {
        mMultiViewFragment.getSlots().refreshCameraList();
    }

    //---------------------------
    // Start Listeners Callbacks
    //---------------------------

    @Override
    public void setActionBarVisibility(boolean b) {
        // nothing to do here ...
    }

    @Override
    public void hideSystemUI() {
        ActivityUtilities.hideSystemUI(this);
    }

    @Override
    public void onFaultReport(Fault fault) {
        Log.e(TAG, "Fault report: " + fault.id.name());
    }

    @Override
    public void switchToFullScreen(boolean isFullscreen) {
        // handle the views while changing the modes (multiView/singleView)
        // this is an example how to handle the toolbar
        myToolbar.setVisibility(isFullscreen ? View.GONE : View.VISIBLE);
    }

    //-------------------------
    // END Listeners Callbacks
    //-------------------------

    @Override
    public void onBackPressed() {
        /*
         handle the back button when the state is on single mode
         */
        if (mMultiViewFragment.getState().isFullScreenMode) {
            mMultiViewFragment.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}