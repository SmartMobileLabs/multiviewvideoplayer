# MultiViewVideoPlayer

This is a sample application to demonstrate how to integrate and use the MultiView fragment from the EVO_Android_Player_SDK

**Init The EVO object**

Add `EVO.init(this);` to `onCreate()` method of the Application class.

**Set the EVO provider**

Create the `EVOConfiguration` and pass the credentials to create the EVO instance. Check `[this](https://gitlab.com/SmartMobileLabs/multiviewvideoplayer/-/blob/master/MultiViewVideoPlayer/app/src/main/java/com/smartmobilelabs/multiviewvideoplayer/EVOProvider.java)` 

**Set the SMLMultiViewFragment**

In the layout xml, place this fragment

`<fragment
        android:id="@+id/multi_view_fragment"
        android:name="com.smartmobilelabs.evo.android.playersdk.player.fragment.MultiView.SMLMultiViewFragment"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:tag="MultiView"
        android:background="@android:color/transparent"
        android:layout_below="@+id/toolbar"
        tools:context=".MainActivity"/>`


Then get a reference to the `SMLMultiViewFragment`:

`mMultiViewFragment = (SMLMultiViewFragment) getSupportFragmentManager().findFragmentById(R.id.multi_view_fragment);`

Create a configuration for `SMLMultiViewFragment`, check [here](https://gitlab.com/SmartMobileLabs/multiviewvideoplayer/-/blob/master/MultiViewVideoPlayer/app/src/main/java/com/smartmobilelabs/multiviewvideoplayer/MainActivity.java#L107)

Then pass the configuration to the `SMLMultiViewFragment`, check [here](https://gitlab.com/SmartMobileLabs/multiviewvideoplayer/-/blob/master/MultiViewVideoPlayer/app/src/main/java/com/smartmobilelabs/multiviewvideoplayer/MainActivity.java#L52)

**Set the device ID**

Pass the device ID to the EVO instance. check [This](https://gitlab.com/SmartMobileLabs/multiviewvideoplayer/-/blob/master/MultiViewVideoPlayer/app/src/main/java/com/smartmobilelabs/multiviewvideoplayer/MainActivity.java#L95)
